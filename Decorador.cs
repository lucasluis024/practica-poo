class Decorador: OrdenBase
{
    protected OrdenBase orden;
    public Decorador (OrdenBase orden)
    {
        this.orden=orden;
    }
    public override double CalculoTotalPrecio()
    {
        Console.WriteLine("CALCULO DEL PRECIO TOTAL DESDE LA CLASER DECORADOR");
        return orden.CalculoTotalPrecio();
    }
}