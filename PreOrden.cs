public class PreOrden: OrdenBase
{
    public override double CalculoTotalPrecio()
    {
        Console.WriteLine("Calculo del precio total en una PreOrden");
        return productos.Sum(x=> x.Precio)*0.9;
    }
}