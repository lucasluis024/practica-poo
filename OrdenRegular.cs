using System;
public class OrdenRegular: OrdenBase
{
    public override double CalculoTotalPrecio()
    {
        Console.WriteLine(" Calculo del Precio total de un pedido normal");
        return productos.Sum(x=> x.Precio);
    }
}